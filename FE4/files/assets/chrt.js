var chart = AmCharts.makeChart("chartdiv", {
    "type": "pie",
    "theme": "light",
    "dataProvider": [{
      "book": "IT",
      "rate": 4.3,
      "color": "#ffffff"
    }, {
      "book": "SCIENCE",
      "rate": 3.5,
      "color": "#FFFFFF"
    },{
        "book": "MATH",
        "rate": 2.5,
        "color": "#FF0F0"
      },],
    "valueField": "rate",
    "titleField": "book",
    "fillColorsField": "color",
    "balloon": {
      "fixedPosition": true
    },
    "export": {
      "enabled": true,
      "menu": []
    }
  });
  
  var chart = AmCharts.makeChart("barchart", {
    "type": "serial",
    "theme": "light",
    "marginRight": 70,
    "dataProvider": [{
      "book": "IT",
      "rate": 4.3,
      "color": "#262626"
    }, {
      "book": "MATH",
      "rate": 2.5,
      "color": "#7F7F7F"
    }, {
      "book": "Science",
      "rate": 3.5,
      "color": "#D9D9D9"
    }],
    "valueAxes": [{
      "axisAlpha": 0,
      "position": "left",
      "title": "Books tally"
    }],
    "startDuration": 1,
    "graphs": [{
      "balloonText": "<b>[[category]]: [[value]]</b>",
      "fillColorsField": "color",
      "fillAlphas": 0.9,
      "lineAlpha": 0.2,
      "type": "column",
      "valueField": "rate"
    }],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "book",
    "categoryAxis": {
      "gridPosition": "start",
      "labelRotation": 45
    },
    "export": {
      "enabled": true,
      "divId": "exportdiv"
    }
  
  });